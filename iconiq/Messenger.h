#pragma once

#include	<stdio.h>
#include	"windows.h"
#include	"commctrl.h"

class WinMessenger {
private:
	HWND		_processID;
	HANDLE		_processHandle;

	int			_screenWidth;
	int			_screenHeight;

	void		LoadProcessID			();
	void		LoadProcessHandle		();
	void		CloseProcessHandle		();

	DWORD		Sys_AllocateMem			(int memSize);
	void		Sys_FreeMem				(DWORD memAddress);

	template< typename T >
	T			Sys_ReadMem				(DWORD memAddress);
	template< typename T >
	void		Sys_WriteMem			(DWORD memAddress, T data);
	void		Sys_WriteStr			(DWORD memAddress, char* string);

public:
	void		Init					();

	POINT		Sys_MousePos			();
	POINT		Sys_SceenSize			();

	int			Msg_GetIconsCount		();
	int			Msg_GetSelectedCount	();
	POINT		Msg_GetIconsSpacing		();

	POINT		Msg_GetIconPosition		(int iconIndex);
	RECT		Msg_GetIconRect			(int iconIndex); // RECT = start XY - End XY

	bool		Msg_IsIconSelected		(int iconIndex);

	void		Msg_SetIconPosition		(int iconIndex, int x, int y);

	void		Msg_DeselectIcon		(int iconIndex);
	void		Msg_DeselectAllIcons	();
};