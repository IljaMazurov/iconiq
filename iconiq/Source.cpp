#include "Utils.h"
#include "Messenger.h"

void AntiMouse      (WinMessenger* router);
void NoClick        (WinMessenger* router);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {
    WinMessenger* router = new WinMessenger();

    router->Init();

    //AntiMouse(router);
    NoClick(router);

    return NULL;
}

void AntiMouse      (WinMessenger* router) {
    int iconsCount      = router->Msg_GetIconsCount();
    int padding         = 20;

    while (true) {
        auto mPos = router->Sys_MousePos();
        for (int i = 0; i < iconsCount; i++) {
            auto rect = router->Msg_GetIconRect(i);

            if (mPos.x + padding > rect.left && mPos.x - padding < rect.right) {
                if (mPos.y + padding > rect.top && mPos.y - padding < rect.bottom) {
                    router->Msg_SetIconPosition(i, mPos.x + Utils::Random(-100, 100), mPos.y + Utils::Random(-100, 100));
                }
            }
        }

        Sleep(1000 / 33);
    }
}

void NoClick        (WinMessenger* router) {
    POINT screen = router->Sys_SceenSize();

    while (true) {
        int selected    = router->Msg_GetSelectedCount();

        if (selected) {
            int iconsCount  = router->Msg_GetIconsCount();
            
            for (size_t i = 0; i < iconsCount; i++) {
                if (router->Msg_IsIconSelected  (i)) {
                    router->Msg_SetIconPosition (i, Utils::Random(0, screen.x), Utils::Random(0, screen.y));
                    router->Msg_DeselectIcon    (i);
                }
            }
        }

        Sleep(1000 / 60);
    }
}