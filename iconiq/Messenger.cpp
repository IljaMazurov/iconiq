#include "Messenger.h"

void    WinMessenger::Init                      () {
    LoadProcessID       ();
    LoadProcessHandle   ();

    _screenWidth    = GetSystemMetrics(SM_CXSCREEN);
    _screenHeight   = GetSystemMetrics(SM_CYSCREEN);
}

void    WinMessenger::LoadProcessID             () {
    HWND h      = FindWindowEx(NULL, NULL,  "Progman", "Program Manager");
    if (h) h    = FindWindowEx(h, NULL,     "SHELLDLL_DefView", NULL);
    if (h) h    = FindWindowEx(h, NULL,     "SysListView32",    NULL);

    _processID = h;
}
void    WinMessenger::LoadProcessHandle         () {
    DWORD explorerID;
    GetWindowThreadProcessId(_processID, &explorerID);
    _processHandle = OpenProcess(PROCESS_VM_READ | PROCESS_VM_WRITE | PROCESS_VM_OPERATION, FALSE, explorerID);
}
void    WinMessenger::CloseProcessHandle        () {
    CloseHandle(_processHandle);
}

DWORD   WinMessenger::Sys_AllocateMem           (int memSize) {
    return (DWORD)VirtualAllocEx(_processHandle, NULL, memSize, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);
}
void    WinMessenger::Sys_FreeMem               (DWORD memAddress) {
    VirtualFreeEx(_processHandle, (LPVOID)memAddress, 0, MEM_RELEASE);
}

template< typename T >
T		WinMessenger::Sys_ReadMem				(DWORD memAddress) {
    T data;
    ReadProcessMemory(_processHandle, (LPVOID)memAddress, &data, sizeof(T), NULL);
    return data;
}
template< typename T >
void	WinMessenger::Sys_WriteMem			    (DWORD memAddress, T data) {
    WriteProcessMemory(_processHandle, (LPVOID)memAddress, &data, sizeof(T), NULL);
}
void	WinMessenger::Sys_WriteStr			    (DWORD memAddress, char* string) {
    int strSize = strlen(string) + 1;
    WriteProcessMemory(_processHandle, (LPVOID)memAddress, string, strSize, NULL);
}


POINT   WinMessenger::Sys_MousePos              () {
    POINT data;
    GetCursorPos(&data);
    return data;
}
POINT   WinMessenger::Sys_SceenSize              () {
    POINT data;
    data.x = _screenWidth;
    data.y = _screenHeight;
    return data;
}

int     WinMessenger::Msg_GetIconsCount         () {
    return SendMessage(_processID, LVM_GETITEMCOUNT, 0, 0);
}
int     WinMessenger::Msg_GetSelectedCount      () {
    return SendMessage(_processID, LVM_GETSELECTEDCOUNT, 0, 0);
}
POINT   WinMessenger::Msg_GetIconsSpacing       () {
    DWORD result = SendMessage(_processID, LVM_GETITEMSPACING, 0, 0);

    POINT data;

    data.x = LOWORD(result);
    data.y = HIWORD(result);

    return data;
}

bool    WinMessenger::Msg_IsIconSelected        (int iconIndex) {
    return SendMessage(_processID, LVM_GETITEMSTATE, iconIndex, LVIS_SELECTED) == LVIS_SELECTED;
}
POINT	WinMessenger::Msg_GetIconPosition       (int iconIndex) {
    DWORD address = Sys_AllocateMem(sizeof(POINT));

    SendMessage(_processID, LVM_GETITEMPOSITION, (LPARAM)(int)iconIndex, address);
    POINT data = Sys_ReadMem<POINT>(address);

    Sys_FreeMem(address);
    return data;
}
 RECT	WinMessenger::Msg_GetIconRect           (int iconIndex) {
    DWORD address = Sys_AllocateMem(sizeof(RECT));

    SendMessage(_processID, LVM_GETITEMRECT, (LPARAM)(int)iconIndex, address);
    RECT data = Sys_ReadMem<RECT>(address);

    Sys_FreeMem(address);
    return data;
}

void    WinMessenger::Msg_SetIconPosition       (int iconIndex, int x, int y) {
    SendMessage(_processID, LVM_SETITEMPOSITION, (LPARAM)(int)iconIndex, (LPARAM)MAKELPARAM((int)x, (int)y));
}

void    WinMessenger::Msg_DeselectIcon          (int iconIndex) {
    DWORD address = Sys_AllocateMem(sizeof(LVITEM));

    LVITEM lvi;
    lvi.state       = 0;
    lvi.stateMask   = LVIS_SELECTED;

    Sys_WriteMem(address, lvi);
    SendMessage(_processID, LVM_SETITEMSTATE, (LPARAM)(int)iconIndex, address);
    Sys_FreeMem(address);
}
void    WinMessenger::Msg_DeselectAllIcons      () {
    Msg_DeselectIcon(-1);
}